package com.example

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class CalculatorTest {


    @Test
    fun multiplyWithUnaryMinus() {
        val expression = "-3x5"
        val result = calculate(expression)
        assertEquals(
            -15f,
            result,
            "$expression evaluation error"
        )
    }
}